package test.example.demo.Service.Impl;

import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import test.example.demo.Service.TransactionService;


@SpringBootTest
@AutoConfigureMockMvc
class TransactionServiceImplTest {

    private static final String completed = "7bd710477a2174bc2424030feec46542";
    private static final String canceled = "310a5dc34c393e2154e080809c2c21c6";
    private static final String decline = "df4efeffe327a3ce66394ccf6a256626";
    private static final String not_found = "df4efeffe327a3ce66394ccf6a251111";

    @Autowired
    private TransactionService transactionService;

    @Test
    void testGetTransactionStatusCompleted() throws JSONException {
        String status = transactionService.getTransactionStatus(completed);
        Assertions.assertEquals(status, "COMPLETED", "Incorrect transaction status");
    }

    @Test
    void testGetTransactionStatusCanceled() {
        String status = transactionService.getTransactionStatus(canceled);
        Assertions.assertEquals(status, "CANCELLED", "Incorrect transaction status");
    }

    @Test
    void testGetTransactionStatusDecline() {
        String status = transactionService.getTransactionStatus(decline);
        Assertions.assertEquals(status, "DECLINED", "Incorrect transaction status");
    }

    @Test
    void testGetTransactionStatusNotFound() throws JSONException {
        String status = transactionService.getTransactionStatus(not_found);
        Assertions.assertEquals(status, "404 NOT FOUND", "Incorrect transaction status");
    }
}