package test.example.demo.Controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import test.example.demo.Service.TransactionService;
import static org.mockito.Mockito.doReturn;

import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;


@SpringBootTest
@AutoConfigureMockMvc
class ClientControllerTest {

    private static final String completed = "7bd710477a2174bc2424030feec46542";
    private static final String canceled = "310a5dc34c393e2154e080809c2c21c6";
    private static final String decline = "df4efeffe327a3ce66394ccf6a256626";
    private static final String not_found = "df4efeffe327a3ce66394ccf6a251111";

    @MockBean
    private TransactionService transactionService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("GET /transaction/status/{transactionId} COMPLETED")
    void testTransactionStatusCompleted() throws Exception {
        String result = "COMPLETED";
        doReturn(Optional.of(result).get()).when(transactionService).getTransactionStatus(completed);

        mockMvc.perform(get("/transaction/status/{transactionId}", completed))
                .andExpect(status().isOk())
                .andExpect(content().string("COMPLETED"))
                .andReturn();
    }

    @Test
    @DisplayName("GET /transaction/status/{transactionId} CANCELLED")
    void testTransactionStatusCanceled() throws Exception {
        String result = "CANCELLED";
        doReturn(Optional.of(result).get()).when(transactionService).getTransactionStatus(canceled);
        mockMvc.perform(get("/transaction/status/{transactionId}", canceled))
                .andExpect(status().isOk())
                .andExpect(content().string("CANCELLED"))
                .andReturn();
    }

    @Test
    @DisplayName("GET /transaction/status/{transactionId} DECLINED")
    void testTransactionStatusDecline() throws Exception {
        String result = "DECLINED";
        doReturn(Optional.of(result).get()).when(transactionService).getTransactionStatus(decline);
        mockMvc.perform(get("/transaction/status/{transactionId}", decline))
                .andExpect(status().isOk())
                .andExpect(content().string("DECLINED"))
                .andReturn();
    }

    @Test
    @DisplayName("GET /transaction/status/{transactionId} 404 NOT FOUND")
    void testTransactionStatusNotFound() throws Exception {
        String result = "404 NOT FOUND";
        doReturn(Optional.of(result).get()).when(transactionService).getTransactionStatus(not_found);
        mockMvc.perform(get("/transaction/status/{transactionId}", not_found))
                .andExpect(status().isOk())
                .andExpect(content().string("404 NOT FOUND"))
                .andReturn();
    }


}