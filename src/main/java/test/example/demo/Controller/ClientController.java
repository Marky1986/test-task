package test.example.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import test.example.demo.Service.TransactionService;

@Controller
@RequestMapping(value = "/transaction")
public class ClientController {

    private TransactionService transactionService;

    @Autowired
    public ClientController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping(value = "/status/{transactionId}")
    public ResponseEntity<?> getTransactionStatus(@PathVariable  String transactionId) {
        return ResponseEntity.ok(transactionService.getTransactionStatus(transactionId));
    }
}
