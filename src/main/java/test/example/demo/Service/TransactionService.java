package test.example.demo.Service;

import java.io.IOException;

public interface TransactionService {
    String getTransactionStatus(String transactionId);
}
