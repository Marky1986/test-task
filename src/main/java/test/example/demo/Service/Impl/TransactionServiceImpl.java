package test.example.demo.Service.Impl;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import test.example.demo.Service.TransactionService;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

@Service
public class TransactionServiceImpl implements TransactionService {
    private static final String get_url_trans = "https://app.bigwallet.online/api/v1/transactions/";
    private static final String token_url = "https://app.bigwallet.online/auth/token";
    private static final String username = "test_key";
    private static final String password = "test_secret";

    @Override
    public String getTransactionStatus(String transactionId) {
        String result = "BAD REQUEST";
        String bearerToken = this.generateBearerToken();
        if (bearerToken == null) {
            return result;
        }

        try {
            URL url = new URL(get_url_trans + transactionId);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestProperty("Authorization", "Bearer " + bearerToken);
            con.setRequestProperty("Accept", "application/json");
            con.connect();
            con.disconnect();
            int status = con.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    br.close();
                    JSONObject jsonObject = new JSONObject(sb.toString());
                    result = jsonObject.getString("state");
                    break;
                case 404:
                    result = "404 NOT FOUND";
                    break;
            }
        } catch (IOException ignored) {
        }
        return result;
    }

    private String generateBearerToken() {
        String authString = username + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        String result = "";
        try {
            URL url = new URL(token_url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            con.connect();
            con.disconnect();
            int status = con.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    br.close();
                    JSONObject jsonObject = new JSONObject(sb.toString());
                    result = jsonObject.getString("access_token");
                    return result;
                default:
                    return result;
            }
        } catch (IOException ignored) {
            return result;
        }
   }
}
